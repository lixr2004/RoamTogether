const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV 
})
const db = cloud.database()
function calculateFeatureValue(record1, record2, record1Gender, record2Gender) {
    const INFINITY = 1000000; // 距离超过此值，则特征值为无穷大
    const distance = Math.sqrt(Math.pow(record1.latitude - record2.latitude, 2) + Math.pow
    (record1.longitude - record2.longitude, 2));
    const MAX_DISTANCE = 0.008;
    if (distance > MAX_DISTANCE || record1.date !== record2.date) {
      return INFINITY;
    }
  
    // 时间差值计算（假设time是HH:MM格式）
    const timeDiff = Math.abs(new Date(`1970/01/01 ${record1.time}`).getTime() - new Date(`1970/01/01 ${record2.time}`).getTime()) / (60 * 60 * 1000); // 将时间差转换为小时
    // 标签映射到数值
    const priceMap = { '穷游型': 1, '实惠型': 5, '轻奢型': 9 };
    const paceMap = { '紧凑型': 1, '宽松型': 7 };
    const transportMap = { '骑行': 1, '步行': 4, '打车': 7 };
    const tagScores = { ...priceMap, ...paceMap, ...transportMap };
  
    let record1Scores = { price: 0, pace: 0, transport: 0 };
    let record2Scores = { ...record1Scores };
  
    for (let tag of record1.tags) {
      if (tag in priceMap) record1Scores.price = priceMap[tag];
      else if (tag in paceMap) record1Scores.pace = paceMap[tag];
      else if (tag in transportMap) record1Scores.transport = transportMap[tag];
    }
  
    for (let tag of record2.tags) {
      if (tag in priceMap) record2Scores.price = priceMap[tag];
      else if (tag in paceMap) record2Scores.pace = paceMap[tag];
      else if (tag in transportMap) record2Scores.transport = transportMap[tag];
    }
  
    // 计算价格、节奏、交通方式的欧几里得距离
    let priceDistance, paceDistance, transportDistance;
    // 如果任一记录的价格得分为0，则价格距离为0
    if (record1Scores.price === 0 || record2Scores.price === 0) {
      priceDistance = 0;
    } else {
      priceDistance = Math.pow(record1Scores.price - record2Scores.price, 2);
    }

    // 如果任一记录的节奏得分为0，则节奏距离为0
    if (record1Scores.pace === 0 || record2Scores.pace === 0) {
      paceDistance = 0;
    } else {
      paceDistance = Math.pow(record1Scores.pace - record2Scores.pace, 2);
    }

    // 如果任一记录的交通方式得分为0，则交通方式距离为0
    if (record1Scores.transport === 0 || record2Scores.transport === 0) {
      transportDistance = 0;
    } else {
      transportDistance = Math.pow(record1Scores.transport - record2Scores.transport, 2);
    }
    let featureValue = Math.sqrt(priceDistance + paceDistance + transportDistance) 
    + timeDiff*2;
  
    // 散装tag匹配减分
    const matchingTags = record1.tags.filter(tag => record2.tags.includes(tag) && !(tag in tagScores)).length;
    const MATCH_DEDUCTION = 1; // 每个匹配散装tag减少的特征值
    featureValue -= matchingTags * MATCH_DEDUCTION;
  
    // 性别不符则特征值为无穷大
    const genderTags = ['男', '女'];
    const record1GenderRequire = record1.tags.find(tag => genderTags.includes(tag));
    const record2GenderRequire = record2.tags.find(tag => genderTags.includes(tag));
    
    if (record2GenderRequire && record1Gender !== record2GenderRequire) {
      return INFINITY;
    }
    if (record1GenderRequire && record1GenderRequire !== record2Gender) {
      return INFINITY;
    }
    return featureValue;
}
  

// 云函数入口函数
exports.main = async (event, context) => {
  // 解析传入的数据
  const { openid, date, time, name, address, latitude, longitude, tags } = event

  // 获取posts集合中的所有记录
  const postsResult = await db.collection('posts').get()
  const posts = postsResult.data

  // 存储每条记录与传入数据的特征值
  let features = []

  for (let post of posts) {
    const userResult1 = await db.collection('users').where({openid: post.openid}).get();
    const user1 = userResult1.data.length > 0 ? userResult1.data[0] : null;
    const record1Gender = user1 ? user1.sex : null;

    const userResult2 = await db.collection('users').where({openid: openid}).get();
    const user2 = userResult2.data.length > 0 ? userResult2.data[0] : null;
    const record2Gender = user2 ? user2.sex : null;

    const featureValue = calculateFeatureValue(post, { date, time, latitude, longitude, tags }, record1Gender, record2Gender)

    // 将post数据、特征值和用户数据合并到features数组中
    if (featureValue !== 1000000) {
      // 将post数据、特征值和用户数据合并到features数组中
      features.push({ post: post, featureValue: featureValue, userData: user1 })
    } 
  }

  // 根据特征值排序，并选择权值最小的5个记录
  features.sort((a, b) => a.featureValue - b.featureValue)
  const topMatches = features.slice(0, 5)

  // 返回结果
  return topMatches
}
