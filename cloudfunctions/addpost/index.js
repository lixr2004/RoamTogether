const cloud = require('wx-server-sdk');
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
});
const db = cloud.database();
const postsCollection = db.collection('posts');
exports.main = async (event, context) => {
  const {openid, date, time, name, address, latitude, longitude, content, tags} = event;
  try{
    const addpost = await postsCollection.add({
      data:{
        openid,
        date, 
        time, 
        address,
        name, 
        latitude, 
        longitude, 
        content, 
        tags, 
        createAt: db.serverDate(),
      },
    });

    return{
      code:200,
      msg:'Added successfully',
      data: addpost._id,
    }
  }catch(error){
    console.error('Add failed', error);
    return {
      code: 500,
      msg: 'Add failed',
      error: error,
    };
  } 
}