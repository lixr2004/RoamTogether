const cloud = require('wx-server-sdk');
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
});
const db = cloud.database();
const usersCollection = db.collection('users');
const documentsCollection = db.collection('documents');

exports.main = async (event, context) => {
  const {openid, name ,studentid } = event;
  try{
    const check = await documentsCollection.where({
      name: name,
      studentid:studentid
    }).get();
    if (check.data.length === 0) {
      return {
        code: 404,
        msg: 'No identity record'
      }
    }
    const sex = check.data[0].sex;
    const user = await usersCollection.where({
      openid: openid
    }).update({
      data:{
        name:name,
        studentid:studentid,
        sex:sex
      }
    });

    if (user && user.stats && user.stats.updated){
      return{
        code:200,
        msg:'Certification successful',
        data:user.stats.updated
      };
    }else{
      return{
        code:404,
        msg:'No user found',
      };
    }
  }catch(error){
    console.error('请求出错', error);
    return { code: 404, msg: '内部服务器错误' };
  }
};