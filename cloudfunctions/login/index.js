// 云函数 login/index.js
const cloud = require('wx-server-sdk');
const https = require('https');


cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
});

const db = cloud.database()
const usersCollection = db.collection('users')

exports.main = async (event, context) => {
  const { code, userInfo } = event;
  let appid = "wxc9b773781860e1d7";
  let secret = "b6a77b1cfb87a02b04336e7fadac7232";

  // 使用Promise封装https请求
  const getOpenid = () => {
    return new Promise((resolve, reject) => {
      const url = `https://api.weixin.qq.com/sns/jscode2session?appid=${appid}&secret=${secret}&js_code=${code}&grant_type=authorization_code`;

      https.get(url, (res) => {
        let rawData = '';
        res.on('data', (chunk) => rawData += chunk);
        res.on('end', () => {
          try {
            const parsedData = JSON.parse(rawData);
            resolve(parsedData);
          } catch (e) {
            reject(e);
          }
        });
      }).on('error', (e) => {
        reject(e);
      });
    });
  };

  try {
    const res = await getOpenid();
    console.log('成功获取openid和session_key:', res);
    const openid = res.openid;
    // 处理你的逻辑，例如存储openid到数据库等
    const userquery = await usersCollection.where({openid}).get();
    if (userquery.data.length === 0){
      await usersCollection.add({
        data:{
          openid,
          userInfo,
          name:null,
          studentid:null,
          sex:null
        }
      })
      return{code: 404, msg: '操作成功', data: res, message:'Real name authentication required'};
    }
    else {
      const user = userquery.data[0];
      if(user.name === null){
        return {code: 404, msg: '操作成功', data: res, message:'Real name authentication required'};
      }else{
        return{code: 200, msg: '操作成功', data: res, message:'Real name authentication completed'};
      }
    }
  } catch (error) {
    console.error('请求出错', error);
    return { code: 500, msg: '内部服务器错误' };
  }
};

