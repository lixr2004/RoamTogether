const cloud = require('wx-server-sdk');
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
});
const db = cloud.database();
const postsCollection = db.collection('posts');
const usersCollection = db.collection('users');

exports.main = async (event, context) => {
  // 获取posts集合的所有记录
  const allPostsResult = await postsCollection.get();
  let posts = allPostsResult.data;

  // 打乱记录顺序
  posts = shuffleArray(posts);

  // 如果记录总数超过5，只选择前5条记录
  const batchSize = 5;
  if (posts.length > batchSize) {
    posts = posts.slice(0, batchSize);
  }

  // 并行获取每个post的userInfo
  const postsWithUserInfo = await Promise.all(posts.map(async post => {
    const userResult = await usersCollection.where({openid: post.openid}).get();
    let userInfo = {};
    if (userResult.data.length > 0) {
      userInfo = userResult.data[0].userInfo; // 假设userInfo字段存储用户信息
    }
    // 返回新的对象结构
    return {
      post: post,
      userInfo: userInfo,
    };
  }));

  return {
    success: true,
    data: postsWithUserInfo,
  };
};

// 洗牌函数，用于随机排序数组
function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]]; // 交换元素
  }
  return array;
}
