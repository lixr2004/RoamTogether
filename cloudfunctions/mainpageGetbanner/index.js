// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV // 使用当前环境ID
})

const db = cloud.database()

exports.main = async (event, context) => {
  try {
    // 从云数据库获取所有图片的File ID
    const bannersResult = await db.collection('banners').get()
    const fileIDs = bannersResult.data.map(item => item.fileID) // 假设每条记录中包含fileID字段

    // 为这些File ID生成临时URL
    const tempUrlsResult = await cloud.getTempFileURL({
      fileList: fileIDs,
    })

    // 提取临时URL
    const tempUrls = tempUrlsResult.fileList.map(file => ({
      fileID: file.fileID,
      tempFileURL: file.tempFileURL,
    }))
    const urls = tempUrlsResult.fileList.map(file => file.tempFileURL)

    // 直接返回URL列表
    return {
      success: true,
      urls: urls,
    }
  } catch (error) {
    return {
      success: false,
      errorMessage: error.message,
    }
  }
}
