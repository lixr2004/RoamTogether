// index.js
// const app = getApp()
const {
  envList
} = require('../../envList.js');  
const app = getApp()
Page({
  data: {
    envList,
    selectedEnv: envList[0],
    haveCreateCollection: false,
    navBarHeight: app.globalData.navBarHeight,
    menuRight: app.globalData.menuRight,
    menuTop: app.globalData.menuTop,
    menuHeight: app.globalData.menuHeight,
  },

  // 根据 data-page 标签参数进行跳转页面
  jumpPage(e) {
    wx.navigateTo({
      url: `/pages/${e.currentTarget.dataset.page}/index?envId=${this.data.selectedEnv.envId}`,
    });
  },

  onClickDatabase(powerList) {
    wx.showLoading({
      title: '',
    });
    wx.cloud.callFunction({
      name: 'quickstartFunctions',
      config: {
        env: this.data.selectedEnv.envId
      },
      data: {
        type: 'createCollection'
      }
    }).then((resp) => {
      if (resp.result.success) {
        this.setData({
          haveCreateCollection: true
        });
      }
      this.setData({
        powerList
      });
      wx.hideLoading();
    }).catch((e) => {
      console.log(e);
      this.setData({
        showUploadTip: true
      });
      wx.hideLoading();
    });
  },

  login(e) {
    var that = this
    wx.getUserProfile({
      desc: 'desc',
      success: res => {
        console.log(res)
        var userInfo = res.userInfo
        console.log("Res UserInfo:");
        console.log(userInfo);
        // 上传userInfo到全局变量保存
        app.globalData.userInfo = userInfo;
        console.log("Global UserInfo"); 
        console.log(app.globalData.userInfo); 
          wx.login({
            success (res) {
              console.log('2')
              console.log(res)
              if (res.code) {
                //发起网络请求 需要补写，将 code 也就是 openid 上传到云数据库保存
                wx.cloud.callFunction({
                  name: 'login',
                  data: {
                    code: res.code,
                    userInfo:userInfo
                  },
                  success (res) {
                    console.log("回调成功");
                    console.log(res);
                    app.globalData.openId = res.result.data.openid;
                    if(res.result.code==200){
                      wx.redirectTo({
                        url: '/pages/home/home',
                      })
                    }
                    
                    if(res.result.code==404){
                      wx.navigateTo({
                        url: '/pages/signup/signup',
                      })
                      wx.showToast({
                        title: res.result.msg,
                        icon:"error"
                      })
                    }
                    
                    that.setData({
                      userInfo:res.result
                    })
                    wx.showToast({
                      title: res.result.msg,
                    })
                    /* wx.navigateTo({
                      url: '/pages/home/home',
                    }) */
                    
                  },
                  fail (err) {
                    console.log("Cannot upload this data.");
                    console.log(err);
                    // wx.showToast({
                    //   title: res.data.msg,
                    //   icon:"error"
                    // })
                    // test Jump
                    wx.navigateTo({
                      url: '/pages/signup/signup',
                    })
                    // end testjump
                  }
                  
                })
              } else {
                console.log('登录失败！' + res.errMsg)
              }
            }
          })
      }
    })

  },
});