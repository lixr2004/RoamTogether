// pages/home/home.js
const app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    posts: [], // 存储帖子信息的数组
    curpost: {},
    banners: [],
    tc:false,
    navBarHeight: app.globalData.navBarHeight,
  },

  // 关闭详情小窗界面动作
  onClone(){
    this.setData({
      tc:false
    })
  },    

  // 点击打开详情小窗动作
  onXq: function(e){
    // console.log(e.currentTarget.dataset);
    const currentpost = e.currentTarget.dataset.post;
    this.setData({
      tc:true,
      curpost: currentpost
    })
    // console.log(this.data.curpost);
  },
  
  onLoad: function() {
    console.log("This is the page info of home.");
    // 从后端获取推荐的Posts
    this.fetchPosts();
    // 从后端获取横幅图片 Banners
    this.fetchBanners();
  },

  // 根据 data-page 标签参数进行跳转页面
  jumpPage(e) {
    wx.navigateTo({
      url: `/pages/${e.currentTarget.dataset.page}/${e.currentTarget.dataset.page}`,
    });
  },

  // 从后端获取帖子信息的函数
  fetchPosts: function() {
    var that = this;

    wx.cloud.callFunction({
      name: 'mainpageGetpost',
      success (res) {
        console.log("回调成功");
        console.log(res);
        // 更新帖子信息的数据
        console.log("GET posts:" , res.result.data);
        that.setData({
          posts: res.result.data
        });
      },
      fail: function(error) {
        console.error('获取帖子信息失败:', error);
      }
    })
    // // test 测试环节赋值
    // this.setData({
    //   posts : [
    //     {
    //       id : 1542367894 ,
    //       title: "第一篇帖子",
    //       author: "张三",
    //       date : "2024/03/22",
    //       time : "19:50",
    //       image: "/images/Clock-Time.png",
    //       content : "这是第一篇帖子的内容。",
    //       tags: ["标签1", "标签2", "标签3", "标签4"],
    //       address: "上海路16号乐子城",
    //       name: '乐子城',
    //       latitude: 115.15564,
    //       longitude: 41.1715,
    //       phone: 15448314826,
    //     },
    //     {
    //       id : 1547897482 ,
    //       title: "第二篇帖子",
    //       author: "李四",
    //       date : "2024/04/22",
    //       time : "19:20",
    //       image: "/images/Forest_Road.jpg",
    //       content : "这是第二篇帖子的内容。唔哈哈哈！",
    //       tags: ["标签3", "标签4", "标签5"],
    //       address: "昆明路114号大江山森林公园",
    //       name: '大江山森林公园',
    //       latitude: 115.11564,
    //       longitude: 41.1515,
    //       phone: 15348561896,
    //     }
    //   ]
    // });
    // // end

  },

  fetchBanners: function() {
    var that = this;
    // 发起网络请求获取 banners 数据
    wx.cloud.callFunction({
      name: 'mainpageGetbanner',
      success: function(res) {
        console.log("GETbanner OK:", res);
        that.setData({
          banners: res.result.urls
        });
      },
      fail: function(error) {
        console.log("GETbanner Error:", error);
        console.error('获取 banners 数据失败:', error);
      }
    });
    console.log("Get banners: ", this.data.banners);
    // // test 测试环节赋值
    // this.setData({
    //   banners : [
    //     "/images/banners/test_banner.jpg",
    //     "/images/banners/test_banner_color.png",
    //   ]
    // });
    // // end
  },

  // // 暂时弃用
  // goToPostDetail: function(event) {
  //   // 获取点击的帖子的 id
  //   var postId = event.currentTarget.dataset.id;
  //   // 跳转到帖子详情页，并传递帖子的 id 或其他必要的参数
  //   console.log(postId);
  //   wx.navigateTo({
  //     url: '/pages/post-detail/post-detail?id=' + postId
  //   });
  // },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})