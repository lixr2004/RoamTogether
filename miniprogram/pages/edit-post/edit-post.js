// pages/edit-post/edit-post.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userpost: {},
    postContent: '',
    phone: '',
    year:app.globalData.year,
    month:app.globalData.month,
    day:app.globalData.day,
    hour:app.globalData.hour,
    minute:app.globalData.minute,
    selectedTags:app.globalData.select_tags,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 获取上一页的userpost
    let userpost = wx.getStorageSync('userpost', this.data.userpost);
    this.setData({
      userpost: userpost,
    });
    console.log("Edit Page:Get userpost");
    console.log(userpost);
    // 获取全局变量 userInfo
    console.log("Result Page:Get userInfo");
    this.setData({
      userInfo: app.globalData.userInfo
    });
    console.log(app.globalData.userInfo);
    console.log(this.data.userInfo);
  },

  // Input输入处理
  handleInput: function(e) {
    this.setData({
      postContent: e.detail.value
    });
  },

  // 提交处理
  handleSubmit: function(e) {
    const that = this;
    let postContent = this.data.postContent;
    // if (e.detail.errMsg === 'getPhoneNumber:ok'){
    //   that.setData({
    //     phone: e.detail.phoneNumber
    //   });
    // }
    // else{
    //   console.log("No phone permission.");
    // }
    // // Logging variable
    // console.log("Get phone number:");
    // console.log(e.detail);
    console.log("Upload post:");
    console.log({
      date: that.data.userpost.date,
      time: that.data.userpost.time,
      content: that.data.postContent,
      tags: that.data.userpost.selectedTags,
      address: that.data.userpost.address,
      name: that.data.userpost.name,
      latitude: that.data.userpost.latitude,
      longitude: that.data.userpost.longitude,
      phone: that.data.phone,
    });
    // end logging 
    // 发送总体post内容
    if (postContent) {
      wx.cloud.callFunction({
        name: 'addpost',
        data: {
          openid: app.globalData.openId,
          date: that.data.userpost.date,
          time: that.data.userpost.time,
          address: that.data.userpost.address,
          name: that.data.userpost.name,
          latitude: that.data.userpost.latitude,
          longitude: that.data.userpost.longitude,
          content: that.data.postContent,
          tags: that.data.userpost.selectedTags,
          phone: that.data.phone,
        },
        success: function(res) {
          console.log("addpost回调成功");
          console.log(res.result);
          wx.showToast({
            title: '上传成功',
            icon: 'success',
            duration: 2000
          })
          wx.navigateBack({
            delta: 5  // 返回上一个界面，如果要返回多个界面，可以增加 delta 的值
          });
          
        },
        fail: function(err) {
          console.error('提交失败：', err);
          wx.showToast({
            title: '上传失败，请重试',
            icon: 'none',
            duration: 2000
          })
          // // test 测试跳转
          // wx.navigateTo({
          //   url: '/pages/home/home',
          // });
          // // end
  
        },
      });
    } else {
      wx.showToast({
        title: '请输入帖子内容',
        icon: 'none'
      });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})