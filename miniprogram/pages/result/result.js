const {
  envList
} = require('../../envList.js');
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    posts: [],
    curpost: {},
    userpost: {},
    userInfo: {},
    tc:false,
    navBarHeight: app.globalData.navBarHeight,
    envList,
    selectedEnv: envList[0],
    haveCreateCollection: false,
    navBarHeight: app.globalData.navBarHeight,
    menuRight: app.globalData.menuRight,
    menuTop: app.globalData.menuTop,
    menuHeight: app.globalData.menuHeight,
  },

  onClone(){
    this.setData({
      tc:false
    })
  },    

  onXq: function(e){
    // console.log(e.currentTarget.dataset);
    const currentpost = e.currentTarget.dataset.post;
    this.setData({
      tc:true,
      curpost: currentpost
    })
    // console.log(this.data.curpost);
  },

  // Start Page action
  onLoad: function() {
    // 获取上一页的userpost
    let userpost = wx.getStorageSync('userpost', this.data.userpost);
    this.setData({
      userpost: userpost,
    });
    console.log("Result Page:Get userpost");
    console.log(userpost);
    // 获取全局变量 userInfo
    console.log("Result Page:Get userInfo");
    this.setData({
      userInfo: app.globalData.userInfo
    });
    console.log(app.globalData.userInfo);
    console.log(this.data.userInfo);
    this.fetchPosts();
  },


  // 从后端获取帖子信息的函数
  fetchPosts: function() {
    var that = this;
    // 发起网络请求获取帖子信息
    console.log("USERINFO:", this.data.userInfo);
    console.log("Upload post details:", {
      openid: app.globalData.openId,
      date: that.data.userpost.date,
      time: that.data.userpost.time,
      address: that.data.userpost.address,
      latitude: that.data.userpost.latitude,
      longitude: that.data.userpost.longitude,
      name: that.data.userpost.name,
      tags: that.data.userpost.selectedTags,
    },);
    wx.cloud.callFunction({
      name: 'match',
      data : {
        openid: app.globalData.openId,
        date: that.data.userpost.date,
        time: that.data.userpost.time,
        address: that.data.userpost.address,
        latitude: that.data.userpost.latitude,
        longitude: that.data.userpost.longitude,
        name: that.data.userpost.name,
        tags: that.data.userpost.selectedTags,
      },
      success: function(res) {
        console.log("Successful get match posts:", res);
        // 更新帖子信息的数据
        that.setData({
          posts: res.result
        });
      },
      fail: function(error) {
        console.error('获取帖子信息失败:', error);
      }
    });
    // // test 测试环节赋值
    // this.setData({
    //   posts : [
    //     {
    //       id : 1542367894 ,
    //       title: "第一篇帖子",
    //       author: "张三",
    //       summary: "这是第一篇帖子的摘要内容。",
    //       date : "2024/03/22",
    //       time : "19:50",
    //       image: "/images/Clock-Time.png",
    //       content : "这是第一篇帖子的内容。",
    //       tags: ["标签1", "标签2", "标签3", "标签4"],
    //       address: "上海路16号乐子城",
    //       name: '乐子城',
    //       latitude: 115.15564,
    //       longitude: 41.1715,
    //       phone: 15448314826,
    //     },
    //     {
    //       id : 1547897482 ,
    //       title: "第二篇帖子",
    //       author: "李四",
    //       summary: "这是第二篇帖子的摘要内容。",
    //       date : "2024/04/22",
    //       time : "19:20",
    //       image: "/images/Forest_Road.jpg",
    //       content : "这是第二篇帖子的内容。唔哈哈哈！",
    //       tags: ["标签3", "标签4", "标签5"],
    //       address: "昆明路114号大江山森林公园",
    //       name: '大江山森林公园',
    //       latitude: 115.11564,
    //       longitude: 41.1515,
    //       phone: 15348561896,
    //     }
    //   ]
    // });
    // // end

  },

  // 按键跳转到主页
  navigateToHome: function() {
    wx.showModal({
      title: '提示',
      content: '确认要跳转到主页吗？所有已填内容会清空',
      success: function(res) {
        if (res.confirm) {
          wx.navigateBack({
            delta: 4  // 返回上一个界面，如果要返回多个界面，可以增加 delta 的值
          });          
        } else if (res.cancel) {
          console.log('用户点击取消');
        }
      }
    });
  },

  // 按键跳转到编辑帖子并发布
  jumpToEdit: function(e){
    wx.setStorageSync('userpost', this.data.userpost);
    wx.navigateTo({
      url: '/pages/edit-post/edit-post',
    });
  },

  submit(){
    wx.navigateTo({
      url: '../edit-post/edit-post',
    });
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})