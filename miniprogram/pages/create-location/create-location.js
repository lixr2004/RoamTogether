const app = getApp()
// pages/create-location/create-location.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    date: '',
    time: '',
    location: null,  // 选择的地点
  },

  // 选择地点函数
  onChooseLocation: function() {
    const that = this;
    wx.chooseLocation({
      success: function(res) {
        that.setData({
          location: res,
        });
      },
    });
    
  },

  // 地点传递函数
  onSubmit: function() {
    const that = this;  // 保存 this 的值

    if (!this.data.location) {
      return;
    }
    console.log("Location:");
    console.log(this.data.location);
    // 导航到 选择 tags 界面，并且传递参数
    wx.navigateTo({
      url: '/pages/create-tags/create-tags?date=' + that.data.date + '&time=' + that.data.time + '&address=' + encodeURIComponent(that.data.location.address) + '&latitude=' + that.data.location.latitude + '&longitude=' + that.data.location.longitude + '&name=' + encodeURIComponent(that.data.location.name),
    });
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // 接受来自时间日期选择页面的 date 和 time
    this.setData({
      date: options.date,
      time: options.time,
    });
    console.log("Received:",this.data.date, this.data.time);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})