// pages/signup/signup.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nameString : "", 
    idValue : "",
    navBarHeight: app.globalData.navBarHeight,
    menuRight: app.globalData.menuRight,
    menuTop: app.globalData.menuTop,
    menuHeight: app.globalData.menuHeight,
  },

  // 提交学号，姓名信息
  sendData() {
    let that = this;
    // 在此处发送 jsonData 到服务器
    wx.cloud.callFunction({
      name: 'authentication',
      data: {
        openid: app.globalData.openId,
        name: that.data.nameString,
        studentid: that.data.idValue
      },
      success (res) {
        console.log("回调成功");
        
        if(res.result.code==200){
          console.log('提交成功：', res);
          wx.navigateTo({
            url: '/pages/home/home',
          })
        }
        
        if(res.result.code==404){
          console.log('提交失败：');
          wx.showModal({
            title: '提示',
              content: '信息不符合，请再次确认',
            success (res) {
              if (res.confirm) {
                //这里是点击确认执行事件
              } else if (res.cancel) {
                    //这里是点击取消执行事件
              }
             }
          })
        }
      },
      fail (err) {
        console.log("Cannot upload data.");
        console.log(err);
        // Test 测试环节跳转
        wx.navigateTo({
          url: '/pages/home/home',
        })
        // end
      }
    })
    
  },

  // 无效函数，用于减少console警告，请忽略
  noneMethod() {
    console.log("Nothing happened");
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})