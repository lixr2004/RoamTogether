Page({
  data: {
    date : '',
    time: '',
    address: '' ,
    latitude: '',
    longitude: '',
    name: '',
    tags: [],  // 从后端获取的所有标签
    searchText: '',  // 用户输入的搜索文本
    filteredTags: [],  // 根据搜索文本过滤的标签
    selectedTags: [],  // 用户选中的标签
    key: 0,  // 用于强制更新视图的 key
    userpost: {},
  },

  

  onLoad: function(options) {
    const date = options.date;
    const time = options.time;
    const address = decodeURIComponent(options.address);
    const latitude = parseFloat(options.latitude);
    const longitude = parseFloat(options.longitude);
    const name = decodeURIComponent(options.name);
    this.setData({
      date: date,
      time: time,
      address: address,
      latitude: latitude,
      longitude: longitude,
      name: name,
    });
    console.log(this.data.date);
    console.log(this.data.time);
    console.log(this.data.address);
    console.log(this.data.latitude);
    console.log(this.data.longitude);
    console.log("name", this.data.name);
    // 在页面加载时从后端获取标签
    wx.request({
      url: 'YOUR_BACKEND_URL',  // 你的后端 URL
      method: 'GET',
      success: (res) => {
        this.setData({
          tags: res.data.tags,
          filteredTags: res.data.tags,
        });
      },
    });
    // Test 数据赋值
    this.setData({
      filteredTags: [
        "穷游型", "实惠型", "轻奢型", "紧凑型", "宽松型", "骑行", "步行", "打车", "美食", "拍照", "ACG", "二次元", "喜欢运动", "腾讯", "网易", "steam", "米哈游", "综艺", "美剧", "日剧", "韩剧", "国产剧", "纪录片", "电影迷", "唱歌", "阅读", "历史"
      ],  
      tags: [
        "穷游型", "实惠型", "轻奢型", "紧凑型", "宽松型", "骑行", "步行", "打车", "美食", "拍照", "ACG", "二次元", "喜欢运动", "腾讯", "网易", "steam", "米哈游", "综艺", "美剧", "日剧", "韩剧", "国产剧", "纪录片", "电影迷", "唱歌", "阅读", "历史"
      ],
    });
    // end 

  },

  onSearchInput: function(e) {
    // 当用户输入搜索文本时，过滤标签
    const searchText = e.detail.value.toLowerCase(); // 转换为小写
    const filteredTags = this.data.tags.filter(tag => tag.toLowerCase().includes(searchText)); // 转换为小写并过滤
    this.setData({
      searchText,
      filteredTags,
    });
  },
  

  clearSearch: function() {
    // 清除搜索文本，并显示所有标签
    this.setData({
      searchText: '',
      filteredTags: this.data.tags,
    });
  },

  selectTag: function(e) {
    const tag = e.currentTarget.dataset.tag;
    if (!this.data.selectedTags.includes(tag)) {
      this.setData({
        selectedTags: this.data.selectedTags.concat(tag),
      });
    } else {
      this.setData({
        selectedTags: this.data.selectedTags.filter(item => item !== tag),
      });
    }
  },
  

  removeTag: function(e) {
    // 当用户点击已选标签时，取消选择该标签，并改变 key 的值
    const tag = e.currentTarget.dataset.tag;
    const selectedTags = this.data.selectedTags.filter(item => item !== tag);
    this.setData({
      selectedTags,
      key: Math.random(),
    });
  },

  submitTags: function() {
    const that = this;
    // 当用户点击提交按钮时，发送选中的标签到后端
    this.setData({
      userpost : {
          date: this.data.date,
          time: this.data.time,
          address: this.data.address,
          latitude: this.data.latitude,
          longitude: this.data.longitude,
          name: this.data.name,
          selectedTags: this.data.selectedTags
      }
    });
    console.log("Sync: userpost");
    console.log(that.data.userpost);
    wx.setStorageSync('userpost', that.data.userpost);
    

    
        // test 测试跳转
        wx.navigateTo({
          url: '/pages/result/result',
        });
        // end
  },
});

