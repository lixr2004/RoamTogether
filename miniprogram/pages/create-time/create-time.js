const app = getApp()
Page({

  /**
   * 页面的初始数据
   */

  data: {
    year: '2024',
    month: '03',
    day: '07',
    time: '12:00', 
    months:[],
    days:[],
    hours:[],
    minutes:[],
    hour:'12',
    minute:'00',
    date: ''
  },


  bindYear: function(e) {
    this.setData({
      year: e.detail.value
    })
  },

  bindDonth: function(e) {
    if(e.detail.value < 9){
     var month =  '0' + (parseInt(e.detail.value) + 1) ;
    }else{
     var month =  parseInt(e.detail.value) + 1;
    }
    this.setData({
      month: month,
    })
  },

  bindDay(e){
    if(e.detail.value < 9){
      var day =  '0' + (parseInt(e.detail.value) + 1) ;
     }else{
      var day =  parseInt(e.detail.value) + 1;
     }
     this.setData({
      day: day,
     })
  },

  bindHour(e){
    if(e.detail.value < 9){
      var hour =  '0' + (parseInt(e.detail.value) + 1) ;
     }else{
      var hour =  parseInt(e.detail.value) + 1;
     }
     this.setData({
      hour: hour,
     })
  },

  bindMinute(e){
    if(e.detail.value < 9){
      var minute =  '0' + (parseInt(e.detail.value)) ;
     }else{
      var minute =  parseInt(e.detail.value);
     }
     this.setData({
      minute: minute,
     })
  },


  onSubmit: function() {
    app.globalData.year = this.data.year;
    app.globalData.month = this.data.month;
    app.globalData.day = this.data.day;
    app.globalData.hour = this.data.hour;
    app.globalData.minute = this.data.minute;
    var selectDate = `${this.data.year}-${this.data.month}-${this.data.day}`;
    var selectTime = `${this.data.hour}:${this.data.minute}`;
    this.setData({
      date : selectDate,
      time : selectTime,
    });
    console.log("Select:TIMEDATE", selectTime, selectDate);
    const datetime = `${this.data.date} ${this.data.time}`;
    console.log(datetime);
    const that = this;  // 保存 this 的值
    // 传递 date 和 time 到选择地点界面
    console.log('/pages/create-location/create-location?date=' + that.data.date + '&time=' + that.data.time);
    wx.navigateTo({
      url: '/pages/create-location/create-location?date=' + that.data.date + '&time=' + that.data.time,
    })
        
  },
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
      var months = [];
      for(var i= 1; i<=12;i++){
        months.push(i)
      }
      var days = [];
      for(var i= 1; i<=31;i++){
        days.push(i)
      }
      var hours = [];
      for(var i= 1; i<=24;i++){
        hours.push(i)
      }
      var minutes = [];
      for(var i= 0; i<60;i++){
        minutes.push(i)
      }
      this.setData({
        months:months,
        days:days,
        hours:hours,
        minutes:minutes
      });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})